import React, { useState, useEffect } from 'react';
import './App.css';
import Message from './components/Message';
import MessageForm from './components/MessageForm';

import db from "./config";

function App() {
  const [newMessage, setNewMessage] = useState("");
  const [newUser, setNewUser] = useState("");
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    db.ref("/messages").on('value', (snapshot) => {
      const fbMessages = snapshot.val();
      const convertedMessages = Object.entries(fbMessages || {}).map(([id, message]) => ({
        ...message,
        id
      }));
      setMessages(convertedMessages);
    });
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    const messageObj = {
      user: newUser,
      content: newMessage,
      datetime: Date.now()
    };
    if (newMessage) {
      db.ref('/messages').push(messageObj);
    }
    setNewMessage("");
  };

  return (
    <div className="App">
      {messages.map((message) => (
        <Message key={message.id} message={message} />
      ))}
      <MessageForm
        userName={newUser}
        message={newMessage}
        handleSubmit={handleSubmit}
        handleContentChange={setNewMessage}
        handleUserChange={setNewUser}
      />
    </div>
  );
}

export default App;