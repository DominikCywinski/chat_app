import React from 'react';

function MessageForm({message, userName,handleSubmit, handleContentChange,handleUserChange}){
    return(
        <div>
            <form onSubmit={handleSubmit}>
                <input 
                    type="text" 
                    name="user"
                    value={userName}
                    onChange={(event) => handleUserChange(event.target.value)}
                />
                <input 
                    type="text" 
                    name="content" 
                    value={message}
                    onChange={(event) => handleContentChange(event.target.value)}
                />
                <input type="submit" value="Submit"/>
            </form> 
        </div>
    )
}

export default MessageForm;