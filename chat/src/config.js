import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyAcFef7pZVPv24hi7Wwwwnr9OeC68LYmU0",
    authDomain: "chat-6e863.firebaseapp.com",
    databaseURL: "https://chat-6e863.firebaseio.com",
    projectId: "chat-6e863",
    storageBucket: "chat-6e863.appspot.com",
    messagingSenderId: "143101460503",
    appId: "1:143101460503:web:8b1d1eda8fe46c4d475c52"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);

  const db = firebase.database();

  export default db;